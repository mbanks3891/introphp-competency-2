<?php

require_once 'comp2functions.php';

writeHead("Competency 2", "Required Proficiencies, Part B- CONFIRMATION PAGE");

$artistFormatted=$_GET['artistname'];
$albumFormatted=$_GET['albumname'];


?>

<p>



<?php echo "#" 
. $_GET['albumid'] 
. ", " 
. "<b>$albumFormatted</b>" 
. " by ". "<em>$artistFormatted</em>" 
. " added on " 
. date("l" ) //day textual
. ", " 
. date("F" ) //month textual
. " " 
. date("j" ) //day of month numeric
. ", " 
. date("Y" ) //year four digits
. " " 
. date("g" ) //hour numeric, 12hr format
. ":" 
. date("i" ) //minutes numeric
. " " 
. date("a") //AM or PM lowercase
. "<br><br><br>";

?>

</p>


<?php writeFoot("2B"); ?>