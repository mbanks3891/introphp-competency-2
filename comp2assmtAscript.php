<?php

require_once 'comp2functions.php';

writeHead("Competency 2", "Required Proficiencies, Part A");

///IF THE SUMBIT BUTTON IS PRESSED, RUN THIS PHP
if(isset($_POST['submit']))
{$valid=true;


//////////TEXT BOX///PAGE LINE 1///ALBUM ID////////////////////////////////////////
$albumid = htmlspecialchars(trim($_POST['albumid']));
if(empty($albumid))
{echo "<p class='error'>album id required</p>";
$valid = false;}

//////////TEXT BOX///PAGE LINE 2///ARTIST NAME////////////////////////////////////////
$artistname = htmlspecialchars(trim($_POST['artistname']));
if (empty($artistname))
{echo "<p class='error'>artist name required</p>";
$valid = false;}

//////////TEXT BOX///PAGE LINE 3///ALBUM NAME////////////////////////////////////////
$albumname= htmlspecialchars($_POST['albumname']);
if(empty($albumname))
{echo"<p class='error'>album name required</p>";
$valid=false;}

//////////TEXT BOX///PAGE LINE 4///PRICE////////////////////////////////////////
$price = htmlspecialchars($_POST['price']);
if(empty($price) or !is_numeric($price)) 
{echo"<p class='error'>numeric price required</p>";
$valid=false;}

//////////RADIO BUTTONS///PAGE LINE 5///MEDIA TYPE////////////////////////////////////////
if (isset($_POST['mediatype'])){$type = $_POST['mediatype'];}
else{echo"<p class='error'>media type required</p>";
$valid=false;
$type="";}



//////////CHECKBOXES///PAGE LINE 6///PLAYLIST////////////////////////////////////////
if (isset($_POST['playlist']))
{$playlist=$_POST['playlist'];}
else {echo "<p class='error'>at least one playlist reqired</p>";
$valid=false;
$playlist[0]="";}


//////////DROPDOWN///PAGE LINE 7///GENRE////////////////////////////////////////
$genre=$_POST['genre'];
if($genre==""){echo"<p class='error'>genre required</p>";
$valid=false;}


//////////TEXT BOX///PAGE LINE 8///TRACKS////////////////////////////////////////
$tracks= htmlspecialchars($_POST['tracks']);
if(empty($tracks))
{echo"<p class='error'>tracks required required</p>";
$valid=false;}

if($tracks < 1 or $tracks > 50)
{echo"<p class='error'># of tracks must be in range 1-50</p>";
$valid=false;}



if ($valid){header("Location: comp2partascriptb.php?albumid=$albumid&artistname=$artistname&albumname=$albumname");
	exit();

}

}//end original if(isset($_POST)['submit']))
///IF THE SUMBIT BUTTON WAS NOT PRESSED, RUN THIS PHP (show blank values)

else{
$albumid="";
$artistname="";
$albumname="";
$price="";
$mediatype="";
$playlist[0]="";
$genre="";
$tracks="";	
$type="";
}
?>	




<!--///DISPLAY THE FORM BASED ON RESULTING PHP-->
<form method="post" action="comp2partAscript.php" enctype="multipart/form-data">

<!--//////////TEXT BOX///PAGE LINE 1///ALBUM ID////////////////////////////////////////-->	
<p>
<label for="albumid">Album ID #:</label>
<input type="text" name="albumid" id="albumid" value="<?php echo $albumid;?>">


<!--//////////TEXT BOX///PAGE LINE 2///ARTIST NAME////////////////////////////////////////-->	
<p>
<label for="artistname">Artist Name:</label>
<input type="artistname" name="artistname" id="artistname" value="<?php echo $artistname;?>">
</p>

<!--//////////TEXT BOX///PAGE LINE 3///ALBUM NAME////////////////////////////////////////-->	
<p>
<label for="albumname">Album Name:</label>
<input type="text" name="albumname" id="albumname" value="<?php echo $albumname;?>">
</p>

<!--//////////TEXT BOX///PAGE LINE 4///PRICE////////////////////////////////////////-->	
<p>
<label for="price">Price:</label>
<input type="price" name="price" id="price" value="<?php echo $price;?>">
</p>




<!--/////RADIO BUTTONS///PAGE LINE 5///MEDIA TYPE//////////////////////////////////////////////-->


<p>
Media Type:
<br>
<input type="radio" name="mediatype" id="MPEG Audio File" value="MPEG Audio File"
<?php if($type=="MPEG Audio File"){echo "checked";}?>>
<label for="MPEG Audio File">MPEG Audio File</label>

<input type="radio" name="mediatype" id="Protected AAC Audio File" value="Protected AAC Audio File"
<?php if($type=="Protected AAC Audio File"){echo "checked";}?>>
<label for="Protected AAC Audio File">Protected AAC Audio File</label>
<br>
<input type="radio" name="mediatype" id="Protected MPEG-4 video file" value="Protected MPEG-4 video file"
<?php if($type=="Protected MPEG-4 video file"){echo "checked";}?>>
<label for="Protected MPEG-4 video file">Protected MPEG-4 video file</label>

<input type="radio" name="mediatype" id="Purchased AAC Audio File" value="Purchased AAC Audio File"
<?php if($type=="Purchased AAC Audio File"){echo "checked";}?>>
<label for="Purchased AAC Audio File">Purchased AAC Audio File</label>

<input type="radio" name="mediatype" id="AAC Audio File" value="AAC Audio File"
<?php if($type=="AAC Audio File"){echo "checked";}?>>
<label for="AAC Audio File">AAC Audio File</label>

</p>



<!--///////CHECKBOXES///PAGE LINE 6///PLAYLIST/////////////////////////////////////////////-->

<p>
Playlist:
<br>
<input type="checkbox" name="playlist[]" id="rock" value="rock"
<?php foreach ($playlist as $item1){if($item1=="rock"){echo"checked";}}?>>
<label for="rock">Rock</label>

<input type="checkbox" name="playlist[]" id="jazz" value="jazz"
<?php foreach ($playlist as $item2){if($item2=="jazz"){echo "checked";}}?>>
<label for="jazz">Jazz</label>

<input type="checkbox" name="playlist[]" id="metal" value="metal"
<?php foreach ($playlist as $item3){if($item3=="metal"){echo "checked";}}?>>
<label for="metal">Metal</label>

<input type="checkbox" name="playlist[]" id="alternative" value="alternative"
<?php foreach($playlist as $item4){if($item4=="alternative"){echo "checked";}}?>>	
<label for="alternative">Alternative</label>

<input type="checkbox" name="playlist[]" id="blues" value="blues"
<?php foreach($playlist as $item5){if($item5=="blues"){echo "checked";}}?>>	
<label for="blues">Blues</label>




<!--//////DROP DOWN SELECTION///PAGE LINE 7///GENRE////////////////////////////////////////-->
<p><label for="genre">Genre:</label>
<select name="genre" id="genre">

<option value="">Select a Genre</option>
<option value="Rock"<?php if ($genre=="Rock"){echo "selected";}?>>Rock</option>
<option value="Jazz"<?php if ($genre=="Jazz"){echo "selected";}?>>Jazz</option>
<option value="Metal"<?php if ($genre=="Metal"){echo "selected";}?>>Metal</option>
<option value="Alternative & Punk"<?php if ($genre=="Alternative & Punk"){echo "selected";}?>>Alternative & Punk</option>
<option value="Noise"<?php if ($genre=="Noise"){echo "selected";}?>>Noise</option>
<option value="Blues"<?php if ($genre=="Blues"){echo "selected";}?>>Blues</option>
<option value="Latin"<?php if ($genre=="Latin"){echo "selected";}?>>Latin</option>
<option value="Reggae"<?php if ($genre=="Reggae"){echo "selected";}?>>Reggae</option>
<option value="Pop"<?php if ($genre=="Pop"){echo "selected";}?>>Pop</option>
<option value="Soundtrack"<?php if ($genre=="Soundtrack"){echo "selected";}?>>Soundtrack</option>
</select><!--dont forget to END SELECT-->
</p>




<!--//////////TEXT BOX///PAGE LINE 8///TRACKS////////////////////////////////////////-->
<p>
<label for="tracks"># of Tracks</label>
<input type="text" name="tracks" id="tracks" value="<?php echo $tracks; ?>">
</p>



<!--///////////BUTTON///PAGE LINE 9///////////////////////////////////////////-->
<p>
<input type="submit" name="submit" value="SUMBIT">
</p>
</form>

<?php writeFoot("2A");?>