<?php

require_once 'comp2functions.php';

writeHead("Required Comp 2.6-2.8", "2.6: Get the current date and time
+ 2.7: Format dates and times + 2.8 Convert a string to a date"
);

echo "<p><br>1. Today is " . date('l, F d, Y');
echo "<p><em><u>date() function used formatting args:</u></em>";
echo "<br><b>l</b> (lowercase 'L') - A full textual representation of a day";
echo "<br><b>F</b> - A full textual representation of a month (January through December)";
echo "<br><b>d</b> - The day of the month (from 01 to 31)";
echo "<br><b>Y</b> - A four digit representation of a year";





 

$today = getDate();

echo "<p><br>2. Today is ".  $today['weekday'] . ", " . $today['month'] . " " . $today['mday']. ", ". $today['year'];

$hour = $today['hours'];
echo "<br>2.5 Current hour is " .  $hour;

if($hour >= 6 and $hour < 12) {echo "<p>Good Morning!</p>";}
elseif($hour >= 12 and $hour < 18){echo "<p>Good Afternoon!</p>";}
elseif($hour >= 18 and $hour < 22){echo "<p>Good Evening!</p>";}
else {echo"<p>Sweet Dreams!</p>";}

echo "<p><em><u>getDate() function with no args, calls for curent date via getDate associative array using indexes:</u></em>";
echo "<br><b>weekday</b> - day of the week, ex Saturday";
echo "<br><b>month</b> - month name, ex March";
echo "<br><b>mday</b> - day of month, ex 2";
echo "<br><b>year</b> - year, ex 2019";
echo "<br><b>hours</b> - year, ex 11";








$newyear = mktime(0,0,0,1,1,2019);
$thisday = time();
$diff = $newyear - $thisday;
$days = round($diff/(60*60*24));
echo "<p><br>3. There are $days days left in this year.</p>";

echo "<p><em><u>UNIX Timestamp via mktime()</u></em>";
echo "<br><b>args format:</b>" . " mktime(" . "<em>hour, minute, second, month, day, year, is_dst... all optional</em>" . ")";

writeFoot(2.6);

?>