<?php
require_once 'comp2functions.php';

writeHead("Required Comp 2.1-2.3: User Form", "2.1: Display information on a web page from a PHP script
+ 2.2: Read data from a form
+ 2.3: Perform validation ");

if(isset($_POST['submit'])){$valid=true;

$firstname = htmlspecialchars($_POST['firstname']);
if (empty($firstname))
{echo"<p class='error'>Please enter your first name</p>";$valid=false;}

$lastname=htmlspecialchars($_POST['lastname']);
if(empty($lastname))
{echo"<p class='error'>Please enter your last name</p>";$valid=false;}

$email = htmlspecialchars($_POST['email']);
if(empty($email))
{echo"<p class='error'>Please enter your </p>";$valid=false;}

$username = htmlspecialchars($_POST['username']);
if(empty($username))
{echo"<p class='error'>Please enter your </p>";$valid=false;}

$password = htmlspecialchars($_POST['password']);
if(empty($password))
{echo"<p class='error'>Please enter your </p>";$valid=false;}

$zip = htmlspecialchars($_POST['zipcode']);
if(!is_numeric($zip))
{echo"<p class='error'>Zip code must be numeric </p>";$valid=false;}

if (isset($POST['usertype']))
{$type = $_POST['usertype'];}
else
{echo"<p class='error'>Please select a user type</p>";$valid=false;$type="";}


if (isset($POST['interests']))
{$interests=$_POST['interests'];}
else 
{echo "<p class='error'>Please select at least one interest</p>";$valid=false;$interests[0]="";}

$county=$_POST['county'];
if($county==""){echo"<p class='error'>Please select a county</p>";$valid=false;}///be sure to not have extra bracket


if($valid){
header("Location:labComp2-3b.php?firstname=$firstname$lastname=$lastname");exit();}}


else{
$firstname="";
$lastname="";
$email="";
$username="";
$password="";
$type="";
$interests[0]="";
$county="";
$zip="";	
}

?>




<form method="post" action="labComp2-3.php">
<p>
<label for="firstname">First name</label>
<input type="text" name="firstname" id="firstname" value="<?php echo $firstname;?>">

<label for="lastname">Last name</label>
<input type="text" name="lastname" id="lastname" value="<?php echo $lastname;?>">
</p>

<p>
<label for="email">Email address:</label>
<input type="email" name="email" id="email" value="<?php echo $email;?>">
</p>

<p>
<label for="username">Username:</label>
<input type="text" name="username" id="username" value="<?php echo $username;?>">
</p>

<p>
<label for="password">Password:</label>
<input type="password" name="password" id="password" value="<?php echo $password;?>">
</p>



<!--//START RADIO BUTTONS////////////////////////////////////////////////////-->
<p>
<input type="radio" name="usertype" id="student" value="student"
<?php if($type=="student"){echo"checked";}?>>
<label for="student">Student</label>

<input type="radio" name="usertype" id="instructor" value="instructor"
<?php if($type=="instructor"){echo "checked";}?>>
<label for="instructor">Instructor</label>

<input type="radio" name="usertype" id="tutor" value="tutor"
<?php if($type=="tutor"){echo "checked";}?>>
<label for="tutor">Tutor</label>
</p>
<!--//////////////////////////////////////////////END RADIO BUTTONS/////////-->


<!--///////START CHECKBOX///////////////////////////////////////////////////-->
<p>
<input type="checkbox" name="interests[]" id="html" value="html"
<?php foreach ($interests as $interest){if($interest=="html"){echo"checked";}}?>>
<label for="html">HTML</label>

<input type="checkbox" name="interests[]" id="css" value="css"
<?php foreach ($interests as $interest){if($interest=="css"){echo "checked";}}?>>
<label for="css">CSS</label>

<input type="checkbox" name="interests[]" id="php" value="php"
<?php foreach ($interests as $interest){if($interest=="php"){echo "checked";}}?>>
<label for="php">PHP</label>

<input type="checkbox" name="interests[]" id="mysql" value="mysql"
<?php foreach($interests as $interest){if($interest=="mysql"){echo "checked";}}?>>	
<label for="php">PHP</label>

<input type="checkbox" name="interests[]" id="js" value="js"
<?php foreach($interests as $interest){if($interest=="js"){echo "checked";}}?>>	
<label for="js">JavaScript</label>
</p>
<!--////////////////////////////////////////////////END CHECKBOX/////////////////-->


<p><label for="county">County:</label>
<select name="county" id="County">
<option value="">Select a county</option>
<option value="Dallas"<?php if ($county=="Dallas"){echo "selected";}?>>Dallas</option>
<option value="Collin"<?php if ($county=="Collin"){echo "selected";}?>>Collin</option>
<option value="Tarrant"<?php if ($county=="Tarrant"){echo "selected";}?>>Tarrant</option>
<option value="Denton"<?php if ($county=="Denton"){echo "selected";}?>>Denton</option>
<option value="other"<?php if ($county=="other"){echo "selected";}?>>other</option>
</p>

<p>
<label for="zip">Zip Code</label>
<input type="text" name="zipcode" id="zip" value="<?php echo $zip; ?>">
</p>

<p>
<input type="submit" name="submit" value="Add User">
</p>
</form>

<?php writeFoot(2.1);?>
</body>
</html>