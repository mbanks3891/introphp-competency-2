<?php
require_once 'comp2functions.php';

writeHead("Required Comp 2.4-2.5: User Form", "2.4: Use string functions
+ 2.5: Create a regular expression"
);

if(isset($_POST['submit'])){$valid=true;

$firstname = htmlspecialchars(trim($_POST['firstname']));
if (empty($firstname))
{echo"<p class='error'>Please enter your first name!!!</p>";
$valid=false;}

$lastname=htmlspecialchars(trim($_POST['lastname']));
if(empty($lastname))
{echo"<p class='error'>Please enter your last name!!!</p>";
$valid=false;}

$firstname = ucfirst(strtolower($firstname));
$lastname = ucfirst(strtolower($lastname));





$email = htmlspecialchars($_POST['email']);
if(empty($email))
{echo"<p class='error'>Please enter your email!!!</p>";
$valid=false;}

if (!preg_match('/[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}/',$email)){
	echo "<p class='error'>(Invalid email address)</p>";
}





$username = htmlspecialchars($_POST['username']);
if(empty($username))
{echo"<p class='error'>Please enter your username!!! </p>";
$valid=false;}

if (strlen($username)<6 or strlen($username>12))
{echo "<p class='error'>(Username must be between 6 and 12 characters in length)</p>";
$valid=false;}






$password = htmlspecialchars($_POST['password']);
if(empty($password))
{echo"<p class='error'>Please enter your password!!! </p>";
$valid=false;}

if (!preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,15}$/',$password)) 
{echo "<p class='error'>(Password must have 1 uppercase letter, 1 lowercase letter, 1 number and be 8-15 characters in length)</p>";}

$pwconf=htmlspecialchars(trim($_POST['pwconf']));

if (empty($pwconf)){echo"<p class='error'>Please confirm the password</p>"; $valid=false;}

if (strcmp($password,$pwconf)!=0){echo "<p class='error'>Passwords do not match</p>"; $valid=false;}


$zip = htmlspecialchars($_POST['zipcode']);
if(!is_numeric($zip))
{echo"<p class='error'>Zip code must be numeric!!! </p>";
$valid=false;}

if (!preg_match('/\d{5}(-\d{4})?/',$zip)) {echo "<p class='error'>(Invalid zip code).</p>";}




if (isset($_POST['usertype'])){$type = $_POST['usertype'];}
else{echo"<p class='error'>Please select a user type</p>";
$valid=false;
$type="";}


if (isset($_POST['interests']))
{$interests=$_POST['interests'];}
else {echo "<p class='error'>Please select at least one interest</p>";
$valid=false;
$interests[0]="";}

$county=$_POST['county'];
if($county==""){echo"<p class='error'>Please select a county</p>";
$valid=false;
}///be sure to NOT have extra bracket

if ($county == 'Dallas'){
if (substr($zip,0,2)!='75'){echo "<p class='error'>Zip codes in Dallas county must start with 75.</p>"; $valid=false;
}
}

//add code to understand form checkbox array re: comp4partAregister... 4-25-19-thurs-1446
//..worked as expected... output the names of the genres instead of number.......
foreach($_POST['interests'] as $key=>$value)
{
echo "<br>".$value;
}



if($valid){
header(
"Location:labComp2-5b.php?firstname=$firstname&lastname=$lastname&email=$email&username=$username&password=$password&type=$type");exit();
}
}
else{
$firstname="";
$lastname="";
$email="";
$username="";
$password="";
$type="";
$interests[0]="";
$county="";
$zip="";	
}

?>




<form method="post" action="labComp2-5.php">

<!--//////////TEXT BOX//////////////////////////////////////////////-->	
<p>
<label for="firstname">First name</label>
<input type="text" name="firstname" id="firstname" value="<?php echo $firstname;?>">

<!--//////////TEXT BOX//////////////////////////////////////////////-->	
<label for="lastname">Last name</label>
<input type="text" name="lastname" id="lastname" value="<?php echo $lastname;?>">
</p>

<!--//////////TEXT BOX//////////////////////////////////////////////-->	
<p>
<label for="email">Email address:</label>
<input type="email" name="email" id="email" value="<?php echo $email;?>">
</p>

<!--//////////TEXT BOX//////////////////////////////////////////////-->	
<p>
<label for="username">Username:</label>
<input type="text" name="username" id="username" value="<?php echo $username;?>">
</p>

<!--//////////TEXT BOX//////////////////////////////////////////////-->	
<p>
<label for="password">Password:</label>
<input type="password" name="password" id="password" value="<?php echo $password;?>">
<br>
<label for="pwconf">Confirm Password:</label>
<input type="password" name="pwconf" id="pwconf">
</p>




<!--/////RADIO BUTTONS////////////////////////////////////////////////////-->
<p>

<input type="radio" name="usertype" id="student" value="student"
<?php if($type=="student"){echo "checked";}?>>
<label for="student">Student</label>

<input type="radio" name="usertype" id="instructor" value="instructor"
<?php if($type=="instructor"){echo "checked";}?>>
<label for="instructor">Instructor</label>

<input type="radio" name="usertype" id="tutor" value="tutor"
<?php if($type=="tutor"){echo "checked";}?>>
<label for="tutor">Tutor</label>

</p>



<!--///////CHECKBOXES///////////////////////////////////////////////////-->
<p>
<input type="checkbox" name="interests[]" id="html" value="html"
<?php foreach ($interests as $interest){if($interest=="html"){echo"checked";}}?>>
<label for="html">HTML</label>

<input type="checkbox" name="interests[]" id="css" value="css"
<?php foreach ($interests as $interest){if($interest=="css"){echo "checked";}}?>>
<label for="css">CSS</label>

<input type="checkbox" name="interests[]" id="php" value="php"
<?php foreach ($interests as $interest){if($interest=="php"){echo "checked";}}?>>
<label for="php">PHP</label>

<input type="checkbox" name="interests[]" id="mysql" value="mysql"
<?php foreach($interests as $interest){if($interest=="mysql"){echo "checked";}}?>>	
<label for="mysql">MySQL</label>

<input type="checkbox" name="interests[]" id="js" value="js"
<?php foreach($interests as $interest){if($interest=="js"){echo "checked";}}?>>	
<label for="js">JavaScript</label>




<!--//////DROP DOWN SELECTION//////////////////////////////////////////////-->
<p><label for="county">County:</label>
<select name="county" id="County">

<option value="">Select a county</option>
<option value="Dallas"<?php if ($county=="Dallas"){echo "selected";}?>>Dallas</option>
<option value="Collin"<?php if ($county=="Collin"){echo "selected";}?>>Collin</option>
<option value="Tarrant"<?php if ($county=="Tarrant"){echo "selected";}?>>Tarrant</option>
<option value="Denton"<?php if ($county=="Denton"){echo "selected";}?>>Denton</option>
<option value="other"<?php if ($county=="other"){echo "selected";}?>>other</option>
</select><!--dont forget to END SELECT-->
</p>

<!--//////////TEXT BOX//////////////////////////////////////////////-->
<p>
<label for="zip">Zip Code</label>
<input type="text" name="zipcode" id="zip" value="<?php echo $zip; ?>">
</p>


<!--///////////BUTTON//////////////////////////////////////////////-->
<p>
<input type="submit" name="submit" value="Add User">
</p>
</form>

<?php writeFoot(2.4);?>
</body>
</html>